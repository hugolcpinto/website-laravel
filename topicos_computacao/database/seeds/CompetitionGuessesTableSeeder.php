<?php

use Illuminate\Database\Seeder;

class CompetitionGuessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = ['user_name'=>'Huguera10',
    			 'competition_name'=>'Single Random Number',
    			 'user_guess'=>31
    			];
        $id = \DB::table('competition_guesses')->insertGetId($data);
    }
}
