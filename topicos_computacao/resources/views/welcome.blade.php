<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="/css/welcome_css.css">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Tópicos em Computação
                </div>

                <div class="links">
                    <a href="{{action('AtividadesController@atividade_02')}}">Atividade 02</a>
                    <a href="{{action('Atividade03Controller@index')}}">Atividade 03</a>
                </div>
            </div>
        </div>
    </body>
</html>
