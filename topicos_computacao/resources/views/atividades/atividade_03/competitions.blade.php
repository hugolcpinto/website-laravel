@extends('atividades.atividade_03.layouts.base_layout')

@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="/css/comps_css.css">
@endsection


@section('content')
    <div class="content">
    
        @parent
        
        @component('atividades.atividade_03.components.comp_competitions')
            @slot('name')
                Single Random Number
            @endslot
            @slot('image')
                one_dice.png
            @endslot
        @endcomponent

        @component('atividades.atividade_03.components.comp_competitions')
            @slot('name')
                Two Random Numbers
            @endslot
            @slot('image')
                two_dices.png
            @endslot
        @endcomponent

    </div>
@stop


@section('footer')
    @parent
@stop
