@extends('atividades.atividade_03.layouts.base_layout')

@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="/css/about_css.css">
@endsection

@section('content')
    <div class="content">
    
        @parent

        <div class="profile-padding"></div>
        
        <div class="flex-center position-ref full-height">
            <div class="title m-b-md">
                @include('atividades.atividade_03.subviews.page_not_done_yet')
            </div>
        </div>    
    </div>
@endsection

@section('footer')
    @parent
@stop
