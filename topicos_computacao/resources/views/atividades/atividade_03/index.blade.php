@extends('atividades.atividade_03.layouts.base_layout')

@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="/css/index_css.css">
@endsection

@section('content')
    <div class="content">
    
        @parent
        
        <div class="flex-center position-ref full-height">            
            <div class="title m-b-md subtitle">
                 <u>Welcome to the Competitions Center</u>
                 <h2>A place to host your competititions and face new challanges.</h2>
            </div>
        </div>

    </div>
@endsection

@section('footer')
    @parent
@stop
