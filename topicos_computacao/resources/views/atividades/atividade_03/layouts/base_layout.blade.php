<!DOCTYPE html>
<html>
<head>
	<title>@yield('title', 'Hugo - Tópicos')</title>
	<link rel="stylesheet" type="text/css" href="/css/base_css.css">

	@section('head')
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	@show

</head>
<body>
	
	@section('content')
        <div class="header flex-center">
            <div class="links">
                <a href="{{action('Atividade03Controller@index')}}">Home</a>
                <a href="{{action('Atividade03Controller@profiles')}}">Profiles</a>
                <a href="{{action('Atividade03Controller@competitions')}}">Competitions</a>
                <a href="{{action('Atividade03Controller@competitions_submissions')}}">Submissions</a>
                <a href="{{action('Atividade03Controller@about')}}">About</a>
            </div>
        
        </div>
	@show


	@section('footer')
		<footer class="footer">
            <p>@ All Rights Reserved</p>
        </footer>
	@show

</body>
</html>