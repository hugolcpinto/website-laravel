@extends('atividades.atividade_03.layouts.base_layout')


@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="/css/comp_css.css">
@endsection


@section('content')
    <div class="content">
    
        @parent

        <div class="profile-padding"></div>
        <div class="flex-center position-ref text">
            @if ($name == "Single Random Number")
                <img src="/images/one_dice.png" alt={{$name}} height=224 width=224>
            @else
                <img src="/images/two_dices.png" height=150 width=224>
            @endif
        </div>

        <div class="flex-center position-ref text">
            <p> {{$name}} <br> </p>
        </div>

        <div class="flex-center position-ref text">
            @if ($name == "Single Random Number")
                <p> 
                    <h2> Guess a number between 0 and 100! </h2>
                </p>
            @else
                <p> 
                    <h2> Guess two numbers between 0 and 100! </h2>
                </p>
            @endif
        </div>

        <div class="flex-center position-ref text">
            @if ($name == "Single Random Number")
                <form action="" method="get">
                    <input type="number" name="guessed_number">
                    <input type="submit" name="submit">
                </form>
            @else
                @include('atividades.atividade_03.subviews.page_not_done_yet')
            @endif
        </div>

        <div class="flex-center position-ref text">
            @if(isset($_GET['submit'])) {
                @if($_GET['guessed_number'] == 67)
                    <p> Acertou! </p>
                @else
                    <p> Tente Novamente...</p>
                @endif
            @endif

            <a href="{{action('Atividade03Controller@store_submission', [$name, 'Anonymous User', 42])}}">click aqui</a>
        </div>

    </div>
@stop


@section('footer')
    @parent
@stop
