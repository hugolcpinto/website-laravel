@extends('atividades.atividade_03.layouts.base_layout')

@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="/css/profiles_css.css">
@endsection


@section('content')
    <div class="content">
    
        @parent

        @component('atividades.atividade_03.components.comp_profiles')
            @slot('name')
                Hugo Pinto
            @endslot
            @slot('image')
                perfil.jpg
            @endslot
        @endcomponent

        @component('atividades.atividade_03.components.comp_profiles')
            @slot('name')
                Guest User
            @endslot
            @slot('image')
                guest.png
            @endslot
        @endcomponent

    </div>
@stop


@section('footer')
    @parent
@stop
