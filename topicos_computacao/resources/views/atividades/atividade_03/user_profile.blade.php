@extends('atividades.atividade_03.layouts.base_layout')

@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="/css/usr_profile_css.css">
@endsection


@section('content')
    <div class="content">
    
        @parent

        <div class="profile-padding"></div>
        <div class="flex-center position-ref text">
            @if ($name == "Hugo Pinto")
                <img src="/images/perfil.jpg" alt={{$name}} height=224 width=224>
            @else
                <img src="/images/guest.png" height=224 width=224>
            @endif
        </div>

        <div class="flex-center position-ref text">
                <p> {{$name}} <br> </p>
        </div>
        <div class="flex-left text">
            @if ($name == "Hugo Pinto")
                <p>
                    Age: 21 years old <br>
                    Job: Student <br>
                    Place: Unifal-mg <br>
                    Nationality: brazilian <br>
                </p>
            @else
                <p>
                    Age:  <br>
                    Job:  <br>
                    Place:  <br>
                    Nationality:  <br>
                </p>
            @endif
        </div>

    </div>
@stop

@section('footer')
    @parent
@stop

