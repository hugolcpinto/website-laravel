
<div class="profile-padding"></div>
<div class="flex-center position-ref ">
    <a href="{{action('Atividade03Controller@show_profile', [$name])}}">
        <div class="profile-preview">
            <div class="flex-left text">
                <img src="/images/{{ $image }}" alt={{ $name }} height=100 width=100>
                <p>{{ $name }} <br>
                Go to profile</p>
            </div>
        </div>
    </a>
</div>