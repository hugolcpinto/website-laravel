<div class="competitions-padding"></div>
<div class="flex-center position-ref ">
    
    <a href="{{action('Atividade03Controller@show_competition', [$name])}}">
        <div class="competitions-preview">
            <div class="flex-left text">
                <img src="/images/{{ $image }}" height=100 width=100>
                <p>{{ $name }} <br>
                Go to competition</p>
            </div>
        </div>
    </a>
</div>