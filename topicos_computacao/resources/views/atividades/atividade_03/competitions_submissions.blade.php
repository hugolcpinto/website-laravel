@extends('atividades.atividade_03.layouts.base_layout')

@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="/css/comps_css.css">
@endsection


@section('content')
    <div class="content">
    
        @parent

            @foreach ($data as $sub)
                <p>User: {{ $sub->user_name }}     Competition: {{ $sub->competition_name}}     Guess: {{ $sub->user_guess }} </p>
            @endforeach



    </div>
@stop


@section('footer')
    @parent
@stop
