<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="/css/atv2_css.css">
        <title>{{config('app.name', 'Hugo - Tópicos')}}</title>

    </head>
    <body>
        <div class="content">
                <div class="position-ref full-height flex-center title m-b-md">
                	Hugo Pinto<br>

                	Desenvolvido em 15/10/17
                </div>
        </div>
    </body>
</html>
